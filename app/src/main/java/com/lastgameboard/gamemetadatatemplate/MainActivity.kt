package com.lastgameboard.gamemetadatatemplate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * Template Application for how to specify the Metadata that will allow your
 * game to beautifully display on Gameboard.
 *
 * Important files:
 * - AndroidManifest.xml
 * - strings.xml
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}